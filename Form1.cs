﻿using Concessionario.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concessionario
{
    public partial class Form1 : Form
    {
        private List<Veicolo> catalogoVeicoli = new List<Veicolo>()
        {
            new Veicolo("BMW", "Serie 1", 2000, 150, 44000, 4),
            new Veicolo("Fiat", "Panda", 1000, 75, 10000, 4),
            new Veicolo("Mercedes", "Classe A", 2000, 160, 50000, 4),
            new Veicolo("Fiat", "Doblo", 1200, 80, 18000, 4),
            new Veicolo("Renault", "Clio", 1200, 95, 20000, 4)
        };
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //elencoModelli.DataSource = catalogoVeicoli.OrderBy(v => v.Modello );
            elencoModelli.DataSource = catalogoVeicoli;
            elencoModelli.DisplayMember = "MarcaEModello";
        }
        private void elencoModelli_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var veicoloSelezionato = elencoModelli.SelectedItem as Veicolo;
            var veicoloSelezionato = (Veicolo)elencoModelli.SelectedItem;
            
            textMarca.Text = veicoloSelezionato.Marca;
            textModello.Text = veicoloSelezionato.Modello;
            textPrezzo.Text = veicoloSelezionato.Prezzo.ToString("c",new CultureInfo("it"));
        }
    }
}
