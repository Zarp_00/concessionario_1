﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concessionario.Dominio
{
    public class Veicolo
    {
        //ctor tab tab
        public Veicolo(string marca, string modello, int cilindrata, int cavalli, decimal prezzo, byte ruote)
        {
            Marca = marca;
            Modello = modello;
            Cilindrata = cilindrata;
            Cavalli = cavalli;
            Prezzo = prezzo;
            Ruote = ruote;
        }
        public TipoVeicolo Tipo { get; private set; } = TipoVeicolo.Autovettura;
        public string Marca { get; private set; }
        public string Modello { get; private set; }
        public int Cilindrata { get; private set; }
        public int Cavalli { get; private set; }
        public decimal Prezzo { get; private set; }
        public byte Ruote { get; private set; }
        public void CambiaTipoVeicolo(TipoVeicolo nuovoTipo)
        {
            if(Tipo != nuovoTipo)
            {
                Tipo = nuovoTipo;
            }
        }

        public string MarcaEModello 
        {
            get { return $"{Marca} {Modello}"; }
            //return string.Format("{0} {1}",Marca, Modello);
        }
    }
}
